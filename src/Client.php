<?php

namespace Gaan\Converter;

use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Psr7;
use Gaan\Converter\Subscription;

class Client{
    protected $key;
    protected $client;

    function __construct(string $key) {
        $this->key = $key;
        $this->client = new Guzzle([
            'base_uri' => 'https://converter.blimey.dev',
            'headers' => [
                'Authorization' => 'Bearer ' . $this->key,
                'Accept' => 'application/json',
            ]
        ]);
    }

    /**
     * @param
     * @return 
     */
    public function getSubscriptions()
    {
        $response = $this->client->get('/api/v1/user/plans');

        if($response->getStatusCode() !== 200){
            return false;
        }
        $subscriptions = [];
        foreach(json_decode($response->getBody()->getContents())->data as $subscription){
            $subscriptions[] = new Subscription($subscription);
        }
        
        return $subscriptions;
    }

    /**
     * @param
     * @return 
     */
    public function submit(string $file_path, string $return_url = null)
    {
        $body = [
            'multipart' => [
                [
                    'name'     => 'files',
                    'contents' => Psr7\Utils::tryFopen($file_path, 'r')
                ],
            ]
        ];
        if($return_url){
            $body['multipart'][] = [
                'name' => 'options[endpoint]',
                'contents' => $return_url
                // 'contents' => get_site_url() . '/wp-json/stl-converter/v1/callback'
            ];
        }
        $response = $this->client->post('/api/v1/files/submit', $body);

        if($response->getStatusCode() !== 200){
            return false;
        }

        $body = json_decode($response->getBody()->getContents());
        return $body;
    }

    /**
     * @param
     * @return 
     */
    public function getActivity(int $activityId)
    {
        $response = $this->client->get("/api/v1/files/{$activityId}");
        if($response->getStatusCode() !== 200){
            return false;
        }

        $body = json_decode($response->getBody()->getContents());
        return $body->data;
    }

    /**
     * @param
     * @return 
     */
    public function download(int $id, string $format, string $target_path)
    {
        $this->client->get("/api/v1/files/{$activityId}/download/{$format}", [
            'sink' => $target_path
        ]);
    }
}