<?php

namespace Gaan\Converter;

use stdClass;

class Subscription{

    public $name, $display_name, $price, $expires_at;
    
    function __construct(stdClass $data) {
        $this->name = $data->name;
        $this->display_name = $data->display_name;
        $this->price = $data->price;
        $this->expires_at = $data->expires_at;
    }
}